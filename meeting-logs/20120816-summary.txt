
KDE team meeting summary


1. Roll call
KDE team members present: 
ago, creffet, dilfridge, jmbsvicetto, johu, tampakrap, thev00d00


2. KDE SC stabilization: discuss/vote on the following options:
a) First 4.8.5 as decided in a former meeting, then 4.9.1 / 4.9.2.
b) Skip 4.8.5, start with 4.9.0 / 4.9.1 directly.
c) Other?

After some discussion, a vote resulted in 5 votes for a), 1 vote for b). 4.8.5
has two remaining (upstream) issues (da translation error and a missing theme
for ksplashx) which will be locally addressed in time before stabilization.

Ago suggested that we should follow the Kernel crowd, and only ever stabilize a
late release in every KDE series ("x.x.7/8"). Most of the other team members
were against that idea, since
* we'd find Gentoo-specific problems only very late then (when the large crowd 
  migrates)
* and people will complain if we wait with stable upgrades too long.
As a compromise we settled on "4.9.0 will never go stable, we talk about 4.9.1
when it's out for a while".


3. Solaris patches: We apply many patches to support Solaris, but there seems to
be no prefix keyword. Does anyone know anything about them? If we are supporting
Solaris, Kensington would like to push these patches upstream. Does anyone have
access to a box to test if they are still useful?

Nobody present had any opinion or cared about the solaris stuff. As a result it
was decided to drop the patches. 

On #gentoo-prefix this was ack'ed later by darkside for the prefix team. ryao
noted there that Qt is broken on Prefix at the moment, and that the KDE patches
could become relevant again after that is fixed.


4. KDE stable subproject: Ago proposed a "KDE stable subproject" some time ago
via mail. The idea is that the subproject members test KDE stable candidates on
an otherwise completely stable system as soon as we decide on a stable
candidate, to make fast and bug-free stabilization possible as soon as its
30days in the tree.

Discussion led to various points:
* Ago is mainly thinking of Gentoo-specific stable candidate QA.
* The "stable subproject" obsoletes the (dead) "herd testers" subproject which
  we tried to start up some time ago.
* No quiz required for participation.
* Johu suggested a more upstream-oriented direction ("like kdepim bug day"), 
  which however seems like a different endeavour.
* In the end, there were no objections, Ago takes care of establishing that 
  group of people and updates our site.


5. Bugs

5a. Bug 417235: app-cdr/k3b: Excessive use of REQUIRED_USE
Majority decision was to keep the REQUIRED_USE, but rename lame -> mp3

5b. Bug 430608: cmake-utils.eclass: add support for dev-util/ninja
It was decided to apply the patch and make building with ninja possible; 
however, if the build fails a message about using an unsupported backend
should be logged (if possible).

5c. Bug 427910: app-office/calligra-2.4.3: move fonts to separate packages
and related bug 427914: www-client/rekonq-1.0: please move Nunito-Regular.ttf 
to separate package
Most people present did not really care. Consensus was
* Find out if there is any license problem with the status quo.
* If yes, talk to upstream and try to solve the problem with them.
Nobody volunteered to research the license situation.

5d. Bug 430858: net-libs/libkolabxml-0.8.0[php] fails
The cause here appears to be that FindPHP4.cmake does not look in
/usr/include/php5.*. (and there is no FindPHP5.cmake). This potentially means
that every search for PHP in cmake is broken (though it appears that nothing in
kde-base at least has IUSE="php", explaining this not being caught). Consensus
was to provide upstream with a FindPHP5 module and ask for inclusion. 


6. Open floor

6a. Further discussion about the stable subproject
Tampakrap notes that for recruiting devs a subproject with overlay access is not
the most efficient way, since people tend to be happy with overlay access and 
are reluctant to go through the actual recruiting process. We should try to 
motivate people to become full developers. In addition he offers to mentor
people.

6b. Default KDE_SCM
Johu proposes to switch this to git. Noone opposes.
